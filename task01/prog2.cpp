//Author: Wahyu Priambodo / TMJ 2B / 2207421048
#include<iostream>
#include<string>
using namespace std;

int main() {
	string str; //deklarasi variabel str
	
	cout<<"Program 2: Membalikkan string/teks\n\n";
	cout<<"Masukkan string/teks\t: ";
	cin>>str; //mengambil input karakter dari prompt dan menyimpannya di variabel str
	//getline(cin,str); //input karakter dengan spasi
	int strlen=str.length()-1;

	cout<<"\nSebelum dibalik\t\t: "<<str<<'\n';
	//swapping karakter
	for(int i=strlen; i>=0; i--) {
		for(int j=strlen; j>i-1; j--) {
			char temp=str[i];
			str[i]=str[j];
			str[j]=temp;
		}
	}
	cout<<"Setelah dibalik\t\t: "<<str<<'\n';

	/*
	//Bisa langsung ditampilkan secara terbalik:
	for(int i=strlen; i>=0; i--)
		cout<<str[i]<<"";
	*/
	
	system("pause");
	return 0;
}
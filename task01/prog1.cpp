//Author: Wahyu Priambodo / TMJ 2B / 2207421048
#include<iostream>
using namespace std;

int main() {
	int n[10]; //inisialisasi array n bertipe int dengan size 10
	int len=(sizeof(n)/sizeof(int));
	
	cout<<"Program 1: Mencari nilai max, min, dan rata-rata\n\n";
	cout<<"Masukkan nilai array: \n";
	float AVR=0; //deklarasi variabel AVR
	for(int i=0; i<len; i++) {
		cin>>n[i];
		AVR=AVR+n[i]; //nilai yg diinputkan ke array langsung dijumlahkan dengan variabel AVR
	}
	AVR=AVR/len; //membagi hasil penjumlahan dengan size array n (10)

	int MAX=n[0], MIN=n[0]; //deklarasi MAX dan MIN untuk menyimpan tiap perubahan nilai komparasi
	for(int i=0; i<len; i++) {
		if(n[i]>MAX) MAX=n[i]; //nilai variabel MAX berubah jika nilai di array > nilai MAX
		if(n[i]<MIN) MIN=n[i]; //nilai variabel MIN berubah jika nilai di array < nilai MIN
	}
	
	//menampilkan nilai MAX, MIN, dan nilai rata-rata (AVR) dari array n
	cout<<"\nNilai max\t: "<<MAX<<'\n';
	cout<<"Nilai min\t: "<<MIN<<'\n';
	cout<<"Nilai rata-rata\t: "<<AVR<<'\n';
	
	system("pause");
	return 0;
}
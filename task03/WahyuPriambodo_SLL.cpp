// Author: Wahyu Priambodo / TMJ 2B / 2207421048
#include<iostream>
#include<string>
using namespace std;

struct NodeSLL {
    int id;
    string name;
    NodeSLL *next;
};

NodeSLL *head = NULL, *tail, *curr, *newNode, *del;

void createFirstNode(int &id, string &name) {
    head = new NodeSLL();
    head->id = id;
    head->name = name;
    head->next = NULL;
    tail = head;
}

void pushFront(int &id, string &name) {
    newNode = new NodeSLL();
    newNode->id = id;
    newNode->name = name;
    newNode->next = head;
    head = newNode;
}

void pushBack(int &id, string &name) {
    newNode = new NodeSLL();
    newNode->id = id;
    newNode->name = name;
    newNode->next = NULL;
    tail->next = newNode;
    tail = newNode;
}

void removeFront() {
    if(head != NULL) {
        del = head;
        head = head->next;
        delete del;
    }
    else {
        cerr << "=> Tidak Ada Data (Kosong)!\n";
    }
}

void removeBack() {
    if(head != NULL) {
        del = tail;
        curr = head;
        
        while(curr->next != tail) {
            curr = curr->next;
        }
        
        tail = curr;
        tail->next = NULL;
        delete del;
    }
    else {
        cerr << "=> Tidak Ada Data (Kosong)!\n";
    }
}

void printSLL() {
    if(head == NULL) {
        cerr << "=> Tidak Ada Data (Kosong)!\n";
    }
    
    curr = head;
    while(curr != NULL) {
        cout << "Id\t: " << curr->id << "\n";
        cout << "Name\t: " << curr->name << "\n\n";
        curr = curr->next;
    }
}

int main() {
    int choice;
    int id;
    string name;
    
    cout << "==============================\n";
    cout << "| Single Linked List Program |\n";
    cout << "==============================\n";
    cout << "Pilihan Operasi :\n";
    cout << "0. Buat Node\n";
    cout << "1. Tambah Node di Depan\n";
    cout << "2. Tambah Node di Belakang\n";
    cout << "3. Hapus Node di Depan\n";
    cout << "4. Hapus Node di Belakang\n";
    cout << "5. Tampilkan Node yang Tersedia\n";
    cout << "6. Keluar\n\n";
    
    do {
        cout << "> Pilih operasi\t: "; cin >> choice; cout << "\n";
        
        switch(choice) {
            case 0:
                if(head != NULL) {
                    cerr << "=> Head Sudah Terisi!\n\n";
                }
                else {
                    cout << "> Masukan id\t: "; cin >> id;
                    cout << "> Masukan nama\t: "; cin >> name;
                    createFirstNode(id, name);
                    cout << "\n";
                }
                break;
                
            case 1:
                if(head == NULL) {
                    cerr << "=> Data Head Kosong!\n\n";
                }
                else {
                    cout << "> Masukan id\t: "; cin >> id;
                    cout << "> Masukan nama\t: "; cin >> name;
                    pushFront(id, name);
                    cout << "\n";
                }
                break;
            
            case 2:
                if(head == NULL) {
                    cerr << "=> Data Head Kosong!\n\n";
                }
                else {
                    cout << "> Masukan id\t: "; cin >> id;
                    cout << "> Masukan nama\t: "; cin >> name;
                    pushBack(id, name);
                    cout << "\n";
                }
                break;
                
            case 3:
                removeFront();
                cout << "\n";
                break;
                
            case 4:
                removeBack();
                cout << "\n";
                break;
            
            case 5:
                printSLL();
                cout << "\n";
                break;
                
            case 6:
                return 0;
                break;
            
            default:
                cerr << "=> Pilihan Tidak Tersedia!\n\n";
                system("pause");
        }

    } while(choice < 7);
}
/* TMJ 2B / Anggota Kelompok:
- M. Abian (2207421039)
- Christian (2207421043)
- Wahyu P. (2207421048)
*/

#include<iostream>
#include<string>
using namespace std;

//inisialisasi prototipe Mahasiswa
class Mahasiswa {
private:
	//set beberapa atribut yang melekat dengan obyek mahasiswa, seperti:
	string nama;
	int semester;
	long long nim;
	float nilai_ips;

public:
	//constructor dengan parameter nama dan nim mahasiswa
	Mahasiswa(string _nama, long long _nim) {
        nama = _nama;
        nim = _nim;
    }

    //method untuk mengubah semester mahasiswa
	void set_semester(int _smt) {
        semester = _smt;
    }

    //method untuk mengubah nim mahasiswa
    void set_nim(long long _nim) {
    	nim = _nim;
    }

    //method untuk mengubah nilai ips mahasiswa
    void set_nilai_ips(float _ips) {
        nilai_ips = _ips;
    }

    //method untuk mendapatkan atribut nama mahasiswa
    string get_nama() {
    	return nama;
    }

    //method untuk mendapatkan atribut semester mahasiswa
    int get_semester() {
		return semester;    	
    }

    //method untuk mendapatkan atribut nim mahasiswa
    long long get_nim() {
        return nim;
    }

    //method untuk mendapatkan atribut nilai ips mahasiswa
	double get_nilai_ips() {
        return nilai_ips;
    }

    //method untuk menampilkan seluruh atribut pada obyek mahasiswa
    void print_data() {
    	cout<<"============================="<<'\n';
    	cout<<"\tDATA LENGKAP"<<'\n';
		cout<<"Nama\t\t: "<<nama<<'\n';
		cout<<"NIM\t\t: "<<nim<<'\n';
		cout<<"Semester\t: "<<semester<<'\n';
		cout<<"Nilai IPS\t: "<<nilai_ips<<'\n';
    	cout<<"============================="<<'\n';
    }
};


int main() {
	//inisialisasi sekaligus instansiasi atribut pada obyek mahasiswa
	Mahasiswa mhs1("Abian",2207421039);
	Mahasiswa mhs2("Christian",2207421043);
	Mahasiswa mhs3("Wahyu",2207421048);

	mhs1.set_semester(2); mhs2.set_semester(4); mhs3.set_semester(6);
	mhs1.set_nilai_ips(3.90); mhs2.set_nilai_ips(3.85); mhs3.set_nilai_ips(3.66);

	//menampilkan seluruh nilai atribut pada setiap obyek mahasiswa 
	mhs1.print_data(); mhs2.print_data(); mhs3.print_data();


	return 0;
}
//Author: Wahyu Priambodo / TMJ 2B / 2207421048
#include<iostream>
#include<string>
using namespace std;

struct Nilai {
	int tugas, kuis, mid, uas;
};

struct JamKerja {
	int jam;
};

int main() {
	//soal no. 1
	cout<<"---------------------------------------------\n";
	cout<<"Program 1: Menghitung Nilai Akhir 5 Mahasiswa\n";
	cout<<"---------------------------------------------\n\n";
	
	Nilai n[5];

	for(int i=0; i<5; i++){
		cout<<"Mahasiswa ke-"<<i+1<<'\n';
		cout<<"Masukan nilai tugas: "; cin>>n[i].tugas;
		cout<<"Masukan nilai kuis: "; cin>>n[i].kuis;
		cout<<"Masukan nilai uts: "; cin>>n[i].mid;
		cout<<"Masukan nilai uas: "; cin>>n[i].uas;

		double nilaiAkhir=(n[i].tugas*0.3)+(n[i].kuis*0.1)+(n[i].mid*0.3)+(n[i].uas*0.3);

		char indeks;
		if(nilaiAkhir>85){
			indeks='A';
		}
		else if(nilaiAkhir>70 && nilaiAkhir<=85){
			indeks='B';
		}
		else if(nilaiAkhir>55 && nilaiAkhir<=70){
			indeks='C';
		}
		else if(nilaiAkhir>40 && nilaiAkhir<=55){
			indeks='D';
		}
		else{
			indeks='E';
		}

		cout<<"[i] Nilai akhir mahasiswa ke-"<<i+1<<": "<<nilaiAkhir<<'\n';
		cout<<"[i] Indeks/grade: "<<indeks<<"\n\n";
	}

	//soal no. 2
	cout<<"----------------------------------------------\n";
	cout<<"Program 2: Menghitung Gaji Mingguan 5 Karyawan\n";
	cout<<"----------------------------------------------\n\n";

	JamKerja karyawan[5];

	for(int i=0; i<5; i++){
		cout<<"Karyawan ke-"<<i+1<<'\n';
		cout<<"Masukan jumlah jam kerja: "; cin>>karyawan[i].jam;

		int gaji=6000;
		int totalGaji=(karyawan[i].jam<=40
						? totalGaji=karyawan[i].jam*gaji
						: totalGaji=40*gaji+(karyawan[i].jam-40)*3*gaji);

		cout<<"[i] Gaji karyawan ke-"<<i+1<<": "<<totalGaji<<"\n\n";
	}
	
	system("pause");
	return 0;
}
//Author: Wahyu Priambodo / TMJ 2B / 2207421048
#include<iostream>
#include<string>
using namespace std;

struct Nilai {
	int tugas, kuis, mid, uas;
};

int main() {
	Nilai n;

	//input statis/manual
	n.tugas=70;
	n.kuis=70;
	n.mid=70;
	n.uas=60;

	double nilaiAkhir=(n.tugas*0.3)+(n.kuis*0.1)+(n.mid*0.3)+(n.uas*0.3);
	
	char indeks;
	if(nilaiAkhir>85){
		indeks='A';
	}
	else if(nilaiAkhir>70 && nilaiAkhir<=85){
		indeks='B';
	}
	else if(nilaiAkhir>55 && nilaiAkhir<=70){
		indeks='C';
	}
	else if(nilaiAkhir>40 && nilaiAkhir<=55){
		indeks='D';
	}
	else{
		indeks='E';
	}

	cout<<"Nilai akhir\t\t: "<<nilaiAkhir<<'\n';
	cout<<"Indeks/grade\t: "<<indeks<<'\n';

	// system("pause");
	return 0;
}
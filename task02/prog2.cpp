//Author: Wahyu Priambodo / TMJ 2B / 2207421048
#include<iostream>
#include<string>
using namespace std;

struct JamKerja {
	int jumlahJamKerja;
};

int main() {
	JamKerja jaker;

	jaker.jumlahJamKerja=51; //input statis (manual)

	int lemburan=0;
	int totalGaji=0;

	if(jaker.jumlahJamKerja>40){
		lemburan=jaker.jumlahJamKerja-40;
		jaker.jumlahJamKerja=40;
	}
	else{
		lemburan=0;
	}

	totalGaji=(jaker.jumlahJamKerja*6000)+(lemburan*18000);
	cout<<"Gaji per minggu: "<<totalGaji<<'\n';

	system("pause");
	return 0;
}